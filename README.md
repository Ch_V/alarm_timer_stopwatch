**Доступна русская версия приложения в ветке win_ru.**

# Alarm - Timer - Stopwatch
Simple and useful 3-in-1 application, including 3 tabs: alarm, timer and stopwatch.

All of them can work simultaneously and independent.

![Alarm_En](https://gitlab.com/Ch_V/alarm_timer_stopwatch/uploads/890510cab63128d2b70638f0659f66ca/Alarm_En.png)
---

Press `Esc` or click left mouse button on blinking frame to reset the ring.
    
To elude popup windows with ask for confirmation on some deleting / aborting actions 
use `Shift + left mouse button` instead of only left mouse button.
 
**For correct application work it is necessary to ensure your system sound is on and application isn't terminated.**
But application window can be minimized, reduced in size, fixed on top etc.

### Alarm
You can set and store unlimited number of alarms.

Check checkbox to activate alarm.

Maximum possible time to alarm is 23:59:59 from current time.

Added alarms are automatically live-sorted (closest alarm on top).

### Timer
Simple and useful timer. 

Maximum timer time is 99:59:59.

You can set your own list of presetted timers.

### Stopwatch
Simple and useful stopwatch.

You can record laps, pause and resume stopwatch. Laps data is copy-pasteble (in Excel for instance)

To pause-resume / record-lap you can click left / right mouse button respectively on time label.
In this case it actuates on button *pressing*, not releasind. 

## System requirements
Window 7, 8, 8.1, 10.

Python3 + playsound.

_

It is possible to conform the application to GNU/Linux, but some obstructions may occur due to ttk-styles and icons.

## Installation
1. Install [Python3](https://www.python.org/downloads/).
2. Install [playsound](https://github.com/TaylorSMarks/playsound/) with command `pip install playsound` in command line.
3. Save files of the program to any place you want.
4. For your convenience create a desktop link to `__main__.pyw` file and set the link icon to `icon_off.ico`.
5. ENJOY / PROFIT.

You can replace `ring.mp3` ring sound to any another as you wish.
