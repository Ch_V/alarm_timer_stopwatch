"""Here we are combining our whole application:
mainframe with time-label and tabs for alarm - timer - stopwatch
"""

from collections import deque
import ctypes
from datetime import timedelta
import json
from multiprocessing import Process
import os
import re
import sys
from threading import Thread
from time import sleep
import tkinter as tk
from tkinter import ttk
from tkinter import messagebox as mb

from playsound import playsound

from alarm import AlarmFrame
from common_components import timedelta2iso
from stopwatch import StopwatchFrame
from ttimer import TimerFrame


__version__ = '3.2.5'

# for correct files loading when started from command line from any directory.
os.chdir(os.path.dirname(__file__))

# for pytest import from __init__
sys.path.append((os.getcwd()))

# icon in taskbar for Windows
if os.name == 'nt':
    appid = 'company.product.subproduct.version'
    ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(appid)


class App(tk.Tk):
    """Final multiframe application"""
    def __init__(self):
        super().__init__()
        self.COMPENSATION_DELAY = 0.4    # sound start dilation compensation, in seconds
        self.SMALL_HEIGHT = 69   # height of reduced window
        self.TIMER_HEIGHT = 239  # height of window when timer tab is selected
        self.PADX = 5
        self.PADY = 5
        self.r_sound_ps = None
        self.alpha = 1
        self.opacity_watcher_isalive = False
        self.opacity_watcher_stop = False  # to stop opacity_watcher
        self.ringing = False  # is it ringing now
        self.ringoff = False  # flag to alarm signal reset

        self.style = ttk.Style()
        self.style.configure('countdown.TLabel', font='None 30')  # 33
        self.style.configure('deciseconds.TLabel', font='None 16')  # 33
        self.style.configure('mynotebook.TNotebook', tabposition=tk.N)
        self.style.configure('mynotebook.TNotebook.Tab', font='None 10', padding=[6, 0], tabposition=tk.N)

        self.title('Alarm')
        self.iconbitmap('icon_off.ico')
        self.resizable(False, False)
        self.winsize_index = deque((0, 1))     # for size switching with '^'-button in menu
        self.restore_options()
        self.protocol("WM_DELETE_WINDOW", self.quit_func)
        self.bind('<Escape>', self.ring_off)

        # Menu
        self.main_menu = tk.Menu(self, tearoff=False)
        self.config(menu=self.main_menu)
        self.main_menu.add_command(label='^', command=self.resize)
        self.main_menu.add_command(label='Opacity', command=lambda: Opacity(self))
        self.topmost_var = tk.BooleanVar(value=False)
        self.main_menu.add_checkbutton(label='to top', variable=self.topmost_var, command=self.topmost)


        # Time indicator in frame.
        # Shows time for closest (in time in future) activated alarm (00:00:00 - if no alarm is activated)
        self.frame_countdown = tk.Frame(self, highlightthickness=5)
        self.lbl_countdown = ttk.Label(self.frame_countdown, text='00:00:00', style='countdown.TLabel')
        # binding of switching off of ringing-started flag
        self.ringofbind = self.lbl_countdown.bind('<Button-1>', self.ring_off)
        self.lbl_deciseconds = ttk.Label(self.frame_countdown, text='2', style='deciseconds.TLabel')
        self.lbl_countdown.pack(side=tk.LEFT)
        self.lbl_deciseconds.pack(side=tk.LEFT, anchor=tk.S)
        self.frame_countdown.pack(padx=self.PADX, pady=self.PADY)

        # Tabs
        self.notebook = ttk.Notebook(self, style='mynotebook.TNotebook')
        self.notebook.enable_traversal()
        self.notebook.pack()

        self.frame_add_options = dict(padding=0, underline=0, sticky=tk.NSEW)

        self.alarm_frame = AlarmFrame(self)
        self.notebook.add(self.alarm_frame, text='Alarm', **self.frame_add_options)

        self.timer_frame = TimerFrame(self)
        self.notebook.add(self.timer_frame, text='Timer', **self.frame_add_options)

        self.stopwatch_frame = StopwatchFrame(self)
        self.notebook.add(self.stopwatch_frame, text='Stopwatch', **self.frame_add_options)

        self.notebook.bind('<<NotebookTabChanged>>', self.tab_changed)

        # applying self.options
        self.geometry(self.options.get('window_position', ''))
        self.notebook.select(tab_id=self.options.get('tab_id', None))

        self.lbl_countdown.bind('<Button-1>', self.stopwatch_frame.start_pause, add=self.ringofbind)
        self.lbl_countdown.bind('<Button-3>', self.stopwatch_frame.add_lap)

    def is_any_active(self) -> bool:
        """if there are any activated alarm / timer"""
        return any([any(alarm.isactive for alarm in self.alarm_frame.alarms), self.timer_frame.isactive])

    def opacity_watcher(self, interval=0.1, up_expand=50):
        """Watches for window opacity when opacity is less than 100%.
        When mouse is in -> sets opacity to 100%.
        Used this way because binded for rootwindow <Enter> and <Leave> works bad
        up_expand - area up expansion to cover menu etc.
         """
        def watch():
            if self.opacity_watcher_isalive:
                return None
            while self.alpha < 1 and not self.opacity_watcher_stop:
                self.opacity_watcher_isalive = True
                self.update()
                # Chech if mouse is inside window
                if self.winfo_pointerx() > self.winfo_rootx() and \
                        self.winfo_pointerx() < self.winfo_rootx() + self.winfo_width() and \
                        self.winfo_pointery() > self.winfo_rooty() - up_expand and \
                        self.winfo_pointery() < self.winfo_rooty() + self.winfo_height():
                    self.set_alpha(1)
                else:
                    self.set_alpha()
                sleep(interval)
            self.opacity_watcher_isalive = False

        if self.alpha < 1 and not self.opacity_watcher_isalive:
            th_opacity_watcher = Thread(target=watch)
            th_opacity_watcher.daemon = True
            th_opacity_watcher.start()

    def resize(self, frameless=False, transparent=False):
        """Changes window size when '^' menu button clicking.
        frameless and transparent are old ideas rudiments and there is no need of them really.
        But maybe let them be here...
        """
        if self.winsize_index[0]:
            self.update()
            if self.options['tab_id'] == '.!timerframe':
                self.geometry(f'{self.winfo_width()}x{self.TIMER_HEIGHT}')
            else:
                self.geometry('')
            if frameless: self.overrideredirect(False)
            if transparent: self.attributes('-transparentcolor', '')
        else:
            self.geometry(f'{self.winfo_width()}x{self.SMALL_HEIGHT}')
            if frameless: self.overrideredirect(True)
            if transparent: self.attributes('-transparentcolor', 'SystemButtonFace')

        self.winsize_index.rotate()

    def restore_options(self):
        """Sets values to self.options dictionary without applying them."""
        try:
            with open('options.json', 'r') as file:
                self.options = json.load(file)
        except Exception:
            self.options = {}

    def ring(self, *, interval=0.3, compensation_delay=0):
        """Visual and audio effects function. Is called when application rings."""
        if self.ringing:
            return None

        self.ringing = True
        self.notebook.select(self.who_is_ringing)
        self.alpha = 1
        self.set_alpha(1)

        # Yes, just so (sic!), for normal focus set to application window in Windows
        # (to enable 'Esc'-ringing-reset shortcut working).
        if self.focus_displayof() is None:
            self.deiconify()
            self.iconify()
            self.update()
        self.deiconify()

        if not self.attributes('-topmost'):
            self.attributes('-topmost', True)
            flag = 1
        else:
            flag = 0

        def r_frame():
            """countdown-frame winking function."""
            self.lbl_countdown.config(text='00:00:00')
            while not self.ringoff:
                self.iconbitmap('icon_ring.ico')
                self.frame_countdown.config(highlightbackground='red')
                sleep(interval)
                self.iconbitmap('icon_off.ico')
                self.frame_countdown.config(highlightbackground='SystemButtonFace')
                sleep(interval / 1.6)
            self.ringoff = False
            self.r_sound_ps.terminate()
            self.r_sound_ps = None
            self.ringing = False
            if self.is_any_active():
                self.iconbitmap('icon_on.ico')
            else:
                self.iconbitmap('icon_off.ico')
        th_frame = Thread(target=r_frame)
        th_frame.daemon = True

        self.r_sound_ps = Process(target=r_sound)
        self.r_sound_ps.daemon = True

        self.r_sound_ps.start()
        sleep(compensation_delay)
        th_frame.start()

        if flag:
            self.attributes('-topmost', False)

    def ring_off(self, *event):
        """Is called when ringing reset."""
        if not self.ringoff and self.r_sound_ps:
            self.ringoff = True
            if self.alarm_frame.alarmlines:
                tmp = self.alarm_frame.alarmlines[0].alarm.datetime.time().isoformat()
                self.alarm_frame.add_alarm(tmp[:2], tmp[3:5], tmp[6:8])
                self.alarm_frame.alarmlines[0].destroy()
                del self.alarm_frame.alarmlines[0]
            self.alarm_frame.draw_alarms()

    def set_alpha(self, alpha=None):
        """Sets opacity."""
        if not alpha:
            alpha = self.alpha
        self.attributes('-alpha', alpha)
        self.opacity_watcher()

    def set_countdown(self, tdelta: 'timedelta', verbose=False, rounding='up'):
        """Sets time-showing label value."""
        time = timedelta2iso(tdelta)
        self.lbl_countdown.config(text=time[:-2])
        if verbose:
            self.lbl_deciseconds.config(text=time[-1:])
        else:
            self.lbl_deciseconds.config(text='')

    def tab_changed(self, event):
        """Is calles when tab changing."""
        tab_id = self.notebook.select()
        self.options.update({'tab_id': tab_id})
        tab_name = self.notebook.tab(tab_id, "text")
        self.active_frame = self.nametowidget(tab_id)
        self.title(tab_name)

        self.update()
        if tab_id == '.!timerframe':
            self.geometry(f'{self.winfo_width()}x{self.TIMER_HEIGHT}')
            if self.timer_frame.time_to_end:
                self.set_countdown(self.timer_frame.time_to_end,
                                   verbose=self.timer_frame.TIME_VERBOSE,
                                   rounding=self.timer_frame.TIME_ROUNDING)
            else:
                self.set_countdown(timedelta(0),
                                   verbose=self.timer_frame.TIME_VERBOSE,
                                   rounding=self.timer_frame.TIME_ROUNDING)
        else:
            self.geometry('')
            if tab_id == '.!stopwatchframe':
                self.set_countdown(self.stopwatch_frame.stopwatch_time,
                                   verbose=self.stopwatch_frame.TIME_VERBOSE,
                                   rounding=self.stopwatch_frame.TIME_ROUNDING)
                if self.stopwatch_frame.isactive:
                    self.stopwatch_frame.btn_pause.focus_force()
                else:
                    self.stopwatch_frame.btn_start.focus_force()

    def topmost(self):
        """Sets application topmost and vice versa when 'to top'/'TOP' button clicking in menu."""
        if self.attributes('-topmost'):
            self.attributes('-topmost', False)
            self.main_menu.entryconfig(2, label='to top')
        else:
            self.attributes('-topmost', True)
            self.main_menu.entryconfig(2, label='TOP')

    def quit_func(self):
        """application quit function."""
        if any(alarm.isactive for alarm in self.alarm_frame.alarms) or\
                self.timer_frame.isactive or\
            self.stopwatch_frame.laps or self.stopwatch_frame.isactive:

            res = mb.askyesno(title='Exit',
                              message='''Are you shure you want to quit?
All alarms/timers/stopwatches would not actuate and stopwatch records would be deleted!''',
                              default='no', icon='warning')
            if res:
                pass
            else:
                return None

        # turn off alarms
        self.ringoff = True
        for alarm in self.alarm_frame.alarms:
            alarm.isactive = False

        # save window_position
        self.options.update({'window_position': re.sub(r'\d+x\d+', '', self.geometry())})
        with open('options.json', 'w') as file:
            json.dump(self.options, file)

        self.destroy()
        self.quit()


class Opacity(tk.Toplevel):
    """Window with opacity-setting slider."""
    def __init__(self, master):
        super().__init__()
        self.master = master
        self.attributes('-topmost', True)
        self.overrideredirect(True)
        self.master.update()
        self.master.opacity_watcher_stop = True
        self.geometry(f'+{self.master.winfo_rootx() - 2}+{self.master.winfo_rooty()-26}')
        self.protocol("WM_DELETE_WINDOW", self.on_quite)
        self.alpha = tk.DoubleVar(value=self.master.alpha)
        self.alpha.trace('w', self.set_alpha)
        self.scale = tk.Scale(self, variable=self.alpha, length=self.master.winfo_width(),
                              from_=0.1, to=1, resolution=0.01, showvalue=True,
                              orient=tk.HORIZONTAL, sliderrelief=tk.FLAT,
                              bg='lightgray', highlightbackground='black', highlightthickness=1,
                              troughcolor='white', activebackground='black')

        self.scale.bind('<ButtonRelease-1>', lambda x: (self.destroy(), self.on_quite()))
        self.bind('<Escape>', lambda x: (self.destroy(), self.on_quite()))
        self.bind('<FocusOut>', lambda x: (self.destroy(), self.on_quite()))

        self.scale.pack()
        self.scale.focus_set()

    def set_alpha(self, *args):
        """Sets opacity when self.scale slider moving."""
        self.master.alpha = self.alpha.get()
        self.master.set_alpha()

    def on_quite(self):
        """Starts opacity_watcher when opacyty-set-window closing."""
        self.master.opacity_watcher_stop = False
        self.master.opacity_watcher()


def r_sound(sound='ring.mp3', player=False):
    """Sound play function
    It is here for multiprocessing
    Not threading because of Python-thread terminating complication.
    """
    if player:
        os.system('start ' + sound)
    else:
        while True:
            playsound(sound)
