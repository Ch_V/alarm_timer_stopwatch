"""Alarm - Timer - Stopwatch application"""

from __init__ import *


def check_state():
    """Monitoring function (for debug only)"""
    while True:
        print(
            # '     isactive->', app.timer_frame.isactive,
            # '     abort->', app.timer_frame.abort,
            # '     time_to_end->', app.timer_frame.time_to_end,
            # '     alarms->', app.alarm_frame.alarms,
            # '     options->', app.options,
            # '     is_any_active->', app.is_any_active()
            # '     focus->', app.focus_displayof()
        )
        sleep(1)


monitor_th = Thread(target=check_state)
monitor_th.daemon = True


if __name__ == '__main__':   # this is necessary clause for multiprocessing start of __init__.r_sound()
    app = App()
    # monitor_th.start()
    app.mainloop()
