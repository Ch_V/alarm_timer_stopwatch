"""Alarm-tabframe definition"""

from dataclasses import dataclass
from datetime import datetime, time, timedelta
import json
import os
from threading import Thread
from time import sleep
import tkinter as tk
from tkinter import ttk
from tkinter import messagebox as mb

from common_components import TimeSetters


# for correct files loading when started from command line from any directory.
os.chdir(os.path.dirname(__file__))


class AlarmFrame(tk.Frame):
    """Alarm tabframe."""
    def __init__(self, master):
        super().__init__(master=master)
        self.TIME_VERBOSE = False
        self.TIME_ROUNDING = 'down'
        self.TOPPAD = 1   # extra offset from top of self
        self.PADX = 5
        self.PADY = 5
        self.UPDATE_INTERVAL = 0.1

        self.style = ttk.Style()
        self.style.configure('buttons_alarm.TButton', font='None 12')

        # Read (if alarms exists)
        self.alarms = []
        self.alarmlines = []
        try:
            with open('alarms.json', 'r') as file:
                self.alarms_time = json.load(file)
                self.alarms_time = [time.fromisoformat(i) for i in self.alarms_time]
                # convert from time to datetime with self.add_alarm method
                for i in self.alarms_time:
                    self.add_alarm(alarm_time=i.isoformat())
        except FileNotFoundError:
            pass

        tk.Label(self, height=self.TOPPAD, font='None 1').pack()  # extra offset from top of self

        # Input fields for new alarm
        self.time_setters = TimeSetters(self, start_time='now', tos=(23, 59, 59))
        self.time_setters.pack()
        for setter in self.time_setters.setters():
            setter.bind('<Return>', self.add_alarm)

        # Buttins <Add alarm> and <Delete all alarms>
        self.btn_addtimer = ttk.Button(self, text='Add alarm', width=18, style='buttons_alarm.TButton',
                                       command=lambda: (self.add_alarm(), self.update_alarms_json(), self.draw_alarms()))
        self.btn_cleartimers = ttk.Button(self, text='Delete all alarms', width=18,
                                          style='buttons_alarm.TButton', command=self.clear_alarms)
        self.btn_cleartimers.bind('<Shift-Button-1>', lambda x: self.clear_alarms(x, forced=True))
        self.btn_addtimer.pack(side=tk.TOP, padx=self.PADX, pady=self.PADY)
        self.btn_cleartimers.pack(side=tk.BOTTOM, padx=self.PADX, pady=self.PADY)

        self.draw_alarms()

        th_watcher = Thread(target=self.watcher)
        th_watcher.daemon = True
        th_watcher.start()

    def add_alarm(self, *args, alarm_time=None):
        """Add alarm to alarms list from time-string
        (with duplicates and current time-relation verification).
        """
        # Get data from spinboxes
        if not alarm_time:
            alarm_time = self.time_setters.get_time_string(h_max=23)

        # Comparation with current time
        t = time.fromisoformat(alarm_time)
        if t >= datetime.now().time():
            t = datetime.combine(datetime.today(), t)
        else:
            t = datetime.combine(datetime.today(), t) + timedelta(days=1)

        # Check duplicates and sort
        if t.time() not in (alarm.datetime.time() for alarm in self.alarms):
            self.alarms.append(Alarm(t, False))

        self.draw_alarms()

    def clear_alarms(self, *args, forced=False):
        """Delete all alarms (active and inactive)."""
        if self.alarms:
            if forced:
                res = True
            else:
                res = mb.askyesno(title='All alarms deletion',
                                  message='''Are you shure you want to delete all alarms (including active)?''',
                                  default='no', icon='warning')
            if res:
                for alarm in self.alarms:
                    alarm.isactive = False
                self.alarms = []
                self.update_alarms_json()
                self.draw_alarms()
            else:
                return None

    def draw_alarms(self):
        """Redraw alarm lines based on self.alarms alarms list."""
        for oldalarm in self.alarmlines:
            oldalarm.destroy()
        self.alarmlines = []

        self.alarms.sort(key=lambda x: x.datetime - datetime.now())

        for i, alarm in enumerate(self.alarms):
            self.alarmlines.append(AlarmLine(self, alarm, app=self))
            self.alarmlines[i].pack()

        try:
            if self.master.is_any_active():
                self.master.iconbitmap('icon_on.ico')
            else:
                self.master.iconbitmap('icon_off.ico')
        except AttributeError:
            pass

    def update_alarms_json(self):
        """Refresh json-file with alarms set times list."""
        with open('alarms.json', 'w') as file:
            json.dump([a.datetime for a in self.alarms], file, default=lambda x: x.time().isoformat()[:8])

    def watcher(self, compensation_delay=0):
        """Watch for alarms condition."""
        compensation_delay = self.master.COMPENSATION_DELAY
        while True:
            # Create list of only active alarms
            active_alarms = [alarm for alarm in self.alarms if alarm.isactive]
            active_alarms_timedelta = []
            for alarm in active_alarms:
                active_alarms_timedelta.append(alarm.datetime - datetime.now())

            # Refresh self.lbl_countdown
            if active_alarms:
                if self.master.options['tab_id'] == '.!alarmframe':
                    self.master.set_countdown(active_alarms_timedelta[0],
                                              verbose=self.TIME_VERBOSE,
                                              rounding=self.TIME_ROUNDING)
                if active_alarms_timedelta[0].total_seconds() <= compensation_delay:
                    self.master.who_is_ringing = '.!alarmframe'
                    self.master.ring(compensation_delay=compensation_delay)
                    tmp = self.alarmlines[0].alarm.datetime.time().isoformat()
                    self.alarmlines[0].destroy()
                    del self.alarmlines[0]
                    del self.alarms[0]
                    self.add_alarm(alarm_time=tmp)
                    self.draw_alarms()

            elif self.master.options.get('tab_id') == '.!alarmframe':
                self.master.set_countdown(timedelta(0),
                                          verbose=self.TIME_VERBOSE,
                                          rounding=self.TIME_ROUNDING)

            # move top inactive alarm when current time crosses its set time
            if self.alarms and not self.alarms[0].isactive:
                if (self.alarms[0].datetime - datetime.now()).total_seconds() < 0:
                    self.alarms[0].datetime = self.alarms[0].datetime + timedelta(days=1)
                    self.draw_alarms()

            sleep(self.UPDATE_INTERVAL)


@dataclass
class Alarm:
    """Dataclass for alarms"""
    datetime: datetime = datetime.now()
    isactive: bool = False


class AlarmLine(tk.Frame):
    """Frame-line for single alarm (checkbutton-activation mark, set time, <Delete> button)."""
    def __init__(self, master, alarm, app):
        super().__init__(master=master)
        self.master = master
        self.PADX = 2
        self.alarm = alarm
        self.app = app
        self.isactive = tk.BooleanVar(value=alarm.isactive)
        self.chb = ttk.Checkbutton(self, variable=self.isactive, command=self.update_alarm_activity)
        self.lbl = tk.Label(self, text=alarm.datetime.time(), font='None 11')

        self.lbl.bind('<Button-1>', self.lbl_bind)

        self.btn = ttk.Button(self, text='Delete',
                              command=self.alarm_del)
        self.btn.bind('<Shift-Button-1>', lambda x: self.alarm_del(forced=True))

        self.chb.pack(side=tk.LEFT, padx=self.PADX)
        self.btn.pack(side=tk.RIGHT, padx=self.PADX)
        self.lbl.pack(side=tk.TOP, padx=self.PADX)

    def alarm_del(self, *args, forced=False):
        """Delete alarm with <Delete> button."""
        if self.isactive.get():
            if forced:
                res = True
            else:
                res = mb.askyesno(title='Alarm deletion',
                                  message='''Are you shure you want to delete all alarms (including active)?
Deleted alarm would not ring!!!''',
                                  default='no', icon='warning')
            if res:
                pass
            else:
                return None

        self.alarm.isactive=False
        self.destroy()
        self.app.alarms.remove(self.alarm)
        self.app.update_alarms_json()
        self.app.draw_alarms()

    def lbl_bind(self, event):
        """Refresh alarm state when time indicator mouse clicking."""
        if self.isactive.get():
            self.isactive.set(False)
        else:
            self.isactive.set(True)
        self.update_alarm_activity()

    def update_alarm_activity(self):
        """Refresh alarm state isactive when checkbox clicking."""
        self.alarm.isactive = self.isactive.get()
        self.app.draw_alarms()
