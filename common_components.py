"""Some overall classes an functions"""

from datetime import datetime, timedelta
import math
import os
import time
import tkinter as tk


def timedelta2iso(tdelta: 'timedelta', verbose=True):
    """Convert datetime.timedelta to iso-format
    verbose - include deciseconds.
    """
    if verbose:
        return time.strftime("%H:%M:%S", time.gmtime(tdelta.total_seconds())) + str(tdelta.total_seconds() % 1)[1:3]
    else:
        return time.strftime("%H:%M:%S", time.gmtime(tdelta.total_seconds()))


def isotimestring2timedelta(isotstr):
    """String with format like '00:00:00' to timedelta."""
    return timedelta(0, int(isotstr[:2]) * 60 * 60 + int(isotstr[3:5]) * 60 + int(isotstr[6:]))


class TimeSetters(tk.Frame):
    """Block of time input fields."""
    class TimeSetter(tk.Frame):
        """One elements for input."""
        def __init__(self, master, separator='', to=59, val='00'):
            super().__init__(master=master)
            self.master = master
            self.separator = separator
            self.to = to
            self.FONT = 'None 14'
            self.var = tk.StringVar(value=val)

            self.spinbox = tk.Spinbox(self, from_=0, to=self.to, wrap=True, textvariable=self.var,
                                      font=self.FONT, format='%02.f', width=2, justify=tk.CENTER)
            # self.spinbox.bind('<Return>', self.master.master.master.add_alarm)
            self.spinbox.bind('<MouseWheel>', self.mouse_wheel)
            self.spinbox.pack(side=tk.LEFT, ipadx=4, ipady=1)

            # Add seperator (hh:mm:ss) to the left
            if self.separator:
                tk.Label(self, text=self.separator, font=self.FONT).pack(side=tk.LEFT)

        def mouse_wheel(self, event):
            """Mouse wheel action on spinbox."""
            if os.name == 'nt':
                delta = event.delta // 120
            else:
                delta = event.delta

            self.var.set(str(divmod(int(self.var.get()) + delta, self.to + 1)[1]).zfill(2))
            self.spinbox.focus_force()

    def __init__(self, master, *, start_time='now', tos=(23, 59, 59)):
        super().__init__(master=master)
        if start_time == 'now':
            start_time = datetime.now().time().isoformat()
        else:
            start_time = '00:00:00'
        self.frame_separators = tk.Frame(self)
        self.ent_h = self.TimeSetter(self.frame_separators, separator=':', to=tos[0], val=start_time[0:2])
        self.ent_m = self.TimeSetter(self.frame_separators, separator=':', to=tos[1], val=start_time[3:5])
        self.ent_s = self.TimeSetter(self.frame_separators, to=tos[2], val=start_time[6:8])
        self.ent_h.pack(side=tk.LEFT)
        self.ent_m.pack(side=tk.LEFT)
        self.ent_s.pack(side=tk.LEFT)
        self.frame_separators.pack()

    def get_time_string(self, h_max=99, m_max=59, s_max=59):
        """Return time string from spinboxes.
        *_max sets allowed limits.
        """
        h = self.ent_h.spinbox.get().zfill(2)
        m = self.ent_m.spinbox.get().zfill(2)
        s = self.ent_s.spinbox.get().zfill(2)

        # Check time format
        h, m, s = h[-2:], m[-2:], s[-2:]

        self.ent_h.spinbox.delete(0, tk.END)
        self.ent_h.spinbox.insert(0, h)
        self.ent_m.spinbox.delete(0, tk.END)
        self.ent_m.spinbox.insert(0, m)
        self.ent_s.spinbox.delete(0, tk.END)
        self.ent_s.spinbox.insert(0, s)

        return ':'.join([h, m, s])

    def setters(self):
        """Return spinboxes tuple for easy access from outside."""
        return self.ent_h.spinbox, self.ent_m.spinbox, self.ent_s.spinbox
