"""Stopwatch-tabframe definition"""

from datetime import datetime, timedelta
import re
from threading import Thread
from time import sleep
import tkinter as tk
from tkinter import ttk
import tkinter.messagebox as mb

from common_components import timedelta2iso


class StopwatchFrame(tk.Frame):
    """Stopwatch tabframe."""
    def __init__(self, master):
        super().__init__(master=master)
        self.TIME_VERBOSE = True
        self.TIME_ROUNDING = 'down'
        self.TOPPAD = 1  # extra offset from top of self
        self.PADX = 10
        self.PADY = 5
        self.UPDATE_INTERVAL = 0.1
        self.TREE_COLUMN_WIDTH = (25, 75, 75)

        self.style = ttk.Style()
        self.style.configure('buttons_stopwatch.TButton', font='None 10')
        self.style.configure('green.buttons_stopwatch.TButton', background='lightgreen')
        self.style.configure('yellow.buttons_stopwatch.TButton', background='yellow')
        self.style.configure('red.buttons_stopwatch.TButton', background='#ff7777')

        self.add_time = timedelta(0)
        self.add_lap_time = timedelta(0)
        self.stopwatch_time = timedelta(0)
        self.last_lap = None
        self.lap_time = timedelta(0)
        self.isactive = False  # is stopwatch active
        self.stop = False  # stopwatch stop flag
        self.laps = []

        tk.Label(self, height=self.TOPPAD, font='None 1').pack()  # extra offset from top of self

        # top buttons
        self.top_btns_frame = tk.Frame(self)  # top buttons frame
        self.btn_pause = ttk.Button(self.top_btns_frame, text = 'Pause', style='yellow.buttons_stopwatch.TButton',
                                    command=self.stop_watch)
        self.btn_start = ttk.Button(self.top_btns_frame, text = 'Start', style='buttons_stopwatch.TButton',
                                    command=self.stopwatcher)
        self.btn_lap = ttk.Button(self.top_btns_frame, text = 'Lap', style='buttons_stopwatch.TButton',
                                  command=self.add_lap)

        self.btn_pause.grid(row=0, column=0)
        self.btn_start.grid(row=0, column=0)
        self.btn_lap.grid(row=0, column=1)

        self.top_btns_frame.pack(side=tk.TOP, fill=tk.X, anchor=tk.N)
        self.top_btns_frame.columnconfigure([0, 1], weight=1)

        # results
        self.tree = ttk.Treeview(self, show='headings', columns=('#1', '#2', '#3'), selectmode=tk.EXTENDED, height=1)
        self.tree.column("#1", width=self.TREE_COLUMN_WIDTH[0])
        self.tree.column("#2", width=self.TREE_COLUMN_WIDTH[1])
        self.tree.column("#3", width=self.TREE_COLUMN_WIDTH[2])
        self.tree.heading('#1', text='#')
        self.tree.heading('#2', text='Lap')
        self.tree.heading('#3', text='Time')
        self.tree.pack(side=tk.TOP, fill=tk.BOTH, expand=True, padx=self.PADX, pady=self.PADY)
        self.tree.bind('<Control-c>', self.copy_iteams)
        self.bind('<Button-1>', lambda x: self.tree.selection_remove(self.tree.selection()))

        # reset button
        self.btn_reset = ttk.Button(self, text='Reset', style='buttons_stopwatch.TButton', command=self.reset)
        tk.Label(self, height=self.TOPPAD, font='None 1').pack(side=tk.BOTTOM)  # доп. отступ от нижнего края self
        self.btn_reset.pack(side=tk.BOTTOM)
        self.btn_reset.bind('<Shift-Button-1>', lambda x: self.reset(forced=True))

        self.bind('<Return>', self.start_pause)
        self.focus_force()

    def add_lap(self, *event):
        """Add lap."""
        if self.master.options['tab_id'] == '.!stopwatchframe':
            if self.lap_time == timedelta(0):
                return None
            self.add_lap_time = timedelta(0)
            self.laps.append(
                (len(self.laps) + 1,
                 timedelta2iso(self.lap_time, verbose=self.TIME_VERBOSE),
                 timedelta2iso(self.stopwatch_time, verbose=self.TIME_VERBOSE))
            )
            self.last_lap = datetime.now()
            # this way to prevent application width 1px increase
            if self.laps[-1][0] > 1:
                self.tree.config(height=self.laps[-1][0])
            self.tree.insert('', tk.END, values=self.laps[-1])
            self.tree.column("#1", width=self.TREE_COLUMN_WIDTH[0])
            self.tree.column("#2", width=self.TREE_COLUMN_WIDTH[1])
            self.tree.column("#3", width=self.TREE_COLUMN_WIDTH[2])
            if not self.isactive:
                self.lap_time = timedelta(0)

    def copy_iteams(self, event):
        """Copy results and convert them to Excel pasteble form."""
        self.clipboard_clear()
        res = []
        for item in self.tree.selection():
            res.append(self.tree.item(item))
        res = str([iteam['values'] for iteam in res])

        res = re.sub(r"'\], \[", '\n', res)
        res = re.sub(r"(?:\[\[)|(?:\]\])|'", '', res)
        res = re.sub(r", ", '\t', res)

        self.clipboard_append(res)

    def reset(self, forced=False):
        """Reset stopwatch."""
        if self.stopwatch_time != timedelta(0):
            if forced:
                res = True
            else:
                res = mb.askyesno(title='Stopwatch reset',
                                  message='''Are you shure you want to reset the stopwatch?
Records would be deleted!''',
                                  default='no', icon='warning')
            if res:
                self.isactive = False
                self.stop = True
                self.stopwatch_time = timedelta(0)
                self.add_time = timedelta(0)
                self.lap_time = timedelta(0)
                self.btn_start.tkraise()
                self.master.set_countdown(timedelta(0),
                                          verbose=self.TIME_VERBOSE,
                                          rounding=self.TIME_ROUNDING)
                self.laps = []
                self.tree.delete(*self.tree.get_children())
                self.last_lap = None
                # this way to prevent application width 1px increase
                if self.tree['height'] > 1:
                    self.tree.config(height=1)
                    self.tree.column("#1", width=self.TREE_COLUMN_WIDTH[0])
                    self.tree.column("#2", width=self.TREE_COLUMN_WIDTH[1])
                    self.tree.column("#3", width=self.TREE_COLUMN_WIDTH[2])
                self.btn_lap.config(style='buttons_stopwatch.TButton')
                self.btn_reset.config(style='buttons_stopwatch.TButton')

    def start_pause(self, event):
        """Start and pause stopwatch with mouse clicking on time indicator."""
        if self.master.options['tab_id'] == '.!stopwatchframe':
            if self.isactive:
                self.stop_watch()
            else:
                self.stopwatcher()

    def stop_watch(self):
        """Pause stopwatch."""
        self.stop = True
        self.btn_start.tkraise()
        self.btn_start.focus_force()
        self.btn_lap.config(style='buttons_stopwatch.TButton')
        self.btn_reset.config(style='buttons_stopwatch.TButton')

    def stopwatcher(self):
        """Start stopwatch watcher-daemon."""
        self.start_time = datetime.now()
        self.last_lap = self.start_time
        if not self.add_time:
            self.add_time = timedelta(0)
        def stopwatch():
            self.isactive = True
            self.stop = False
            self.btn_pause.tkraise()
            self.btn_pause.focus_force()
            self.btn_lap.config(style='green.buttons_stopwatch.TButton')
            self.btn_reset.config(style='red.buttons_stopwatch.TButton')
            while not self.stop:
                self.stopwatch_time = datetime.now() - self.start_time + self.add_time
                self.lap_time = datetime.now() - self.last_lap + self.add_lap_time
                if self.master.options['tab_id'] == '.!stopwatchframe':
                    self.master.set_countdown(self.stopwatch_time,
                                              verbose=self.TIME_VERBOSE,
                                              rounding=self.TIME_ROUNDING)
                sleep(self.UPDATE_INTERVAL)
            if self.isactive:
                self.add_time = self.stopwatch_time
                self.add_lap_time = self.lap_time
            self.isactive = False
        th_stopwatcher = Thread(target=stopwatch)
        th_stopwatcher.daemon = True
        if not self.isactive:
            th_stopwatcher.start()
