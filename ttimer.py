"""Timer-tabframe definition"""

from common_components import TimeSetters, isotimestring2timedelta
from datetime import datetime, timedelta
import json
import os
from threading import Thread
from time import sleep
import tkinter as tk
from tkinter import ttk
import tkinter.messagebox as mb


# for correct files loading when started from command line from any directory.
os.chdir(os.path.dirname(__file__))


class TimerFrame(tk.Frame):
    """Timer tabframe."""
    def __init__(self, master):
        super().__init__(master=master)
        self.TIME_VERBOSE = False
        self.TIME_ROUNDING = 'down'
        self.TOPPAD = 1   # extra offset from top of self
        self.PADX = 10
        self.PADY = 5
        self.UPDATE_INTERVAL = 0.1

        self.style = ttk.Style()
        self.style.configure('buttons_timer.TButton', font='None 10')
        self.style.configure('yellow.buttons_timer.TButton', background='yellow')
        self.style.configure('red.buttons_timer.TButton', background='#ff7777')
        self.option_add("*TCombobox*Listbox*Font", "None, 14")

        self.abort = False
        self.isactive = False
        self.time_to_end = None
        # Load timer user presets
        try:
            with open('timers.json', 'r') as file:
                self.timers = json.load(file)
        except FileNotFoundError:
            self.timers = ['00:01:00', '00:02:00', '00:03:00', '00:05:00',
                           '00:10:00', '00:15:00', '00:20:00', '00:30:00']

        tk.Label(self, height=self.TOPPAD, font='None 1').pack()  # extra offset from top of self

        # top buttons
        self.top_btns_frame = tk.Frame(self)  # top buttons frame
        self.btn_pause = ttk.Button(self.top_btns_frame, text = 'Pause', style='yellow.buttons_timer.TButton',
                                    command=self.timer_pause)
        self.btn_start = ttk.Button(self.top_btns_frame, text = 'Start', style='buttons_timer.TButton',
                                    command=self.timer_start)
        self.btn_abort = ttk.Button(self.top_btns_frame, text = 'Cancel', style='buttons_timer.TButton',
                                  command=lambda: self.timer_abort_and_ringoff())
        self.btn_abort.bind('<Shift-Button-1>', lambda x: self.timer_abort_and_ringoff(forced=True))
        self.btn_pause.grid(row=0, column=0)
        self.btn_start.grid(row=0, column=0)
        self.btn_abort.grid(row=0, column=1)
        self.top_btns_frame.pack(side=tk.TOP, fill=tk.X, anchor=tk.N, )
        self.top_btns_frame.columnconfigure([0, 1], weight=1)

        # timer input fields
        self.time_setters = TimeSetters(self, start_time='zero', tos=(99, 59, 59))
        self.time_setters.pack(padx=self.PADX, pady=self.PADY)
        start_times = self.master.options.get('last_used_timer', '00:00:00').split(':')
        for i, setter in enumerate(self.time_setters.setters()):
            setter.bind('<Return>', self.timer_start)
            setter.delete(0, tk.END)
            setter.insert(0, start_times[i])

        # Combobox
        self.combo = ttk.Combobox(self, values=self.timers,
                                  width=8,  font = "None, 14")
        self.combo.insert(0, "Timers: ")
        self.combo.config(state='readonly')
        self.combo.pack(fill=tk.Y, padx=self.PADX, pady=self.PADY)
        self.combo.bind('<<ComboboxSelected>>', self.set_setters_from_combobox)

        # bottom buttons
        self.bottom_btns_frame = tk.Frame(self)  # bottom buttons frame
        self.btn_add = ttk.Button(self.bottom_btns_frame, text = 'Add', style='buttons_timer.TButton',
                                    command=self.add_timer)
        self.btn_del = ttk.Button(self.bottom_btns_frame, text = 'Remove', style='buttons_timer.TButton',
                                    command=self.del_timer)
        self.btn_add.grid(row=0, column=0)
        self.btn_del.grid(row=0, column=1)
        self.bottom_btns_frame.pack(fill=tk.X, side=tk.TOP, anchor=tk.N)
        self.bottom_btns_frame.columnconfigure([0, 1], weight=1)

        tk.Label(self, height=self.TOPPAD, font='None 1').pack()  # extra offset from top of self

    def add_timer(self):
        """Add timer to combobox-list."""
        t = self.time_setters.get_time_string(h_max=99, m_max=59, s_max=59)
        self.timers.append(t)
        self.update_timers()
        self.combo.set(t)

    def del_timer(self):
        """Delete timer from combobox-list."""
        t = self.time_setters.get_time_string(h_max=99, m_max=59, s_max=59)
        if t in self.timers:
            self.timers.remove(t)
            self.update_timers()
            self.combo.set('')

    def set_setters_from_combobox(self, event):
        """Sets to spinbox-setters value from combobox."""
        for i, setter in enumerate(self.time_setters.setters()):
            setter.delete(0, tk.END)
            setter.insert(0, self.combo.get().split(':')[i])

    def timer_abort(self, *event, forced=False):
        """Reset timer."""
        if self.time_to_end:
            if forced:
                res = True
            else:
                res = mb.askyesno(title='Timer reset',
                                  message='''Are you shure you want to cancel the timer?''',
                                  default='no', icon='warning')
            if res:
                self.btn_start.tkraise()
                self.btn_abort.config(style='buttons_timer.TButton')
                self.master.set_countdown(timedelta(0), verbose=self.TIME_VERBOSE, rounding=self.TIME_ROUNDING)
                if self.isactive:
                    self.abort = True

                self.time_to_end = None

    def timer_abort_and_ringoff(self, *event, forced=False):
        """Start timer_abort and ring_off when clicking <Cancel> button."""
        try:
            if self.master.who_is_ringing == '.!timerframe' and not self.time_to_end:
                self.master.ring_off()
                forced=True
        except AttributeError:
            pass
        self.timer_abort(forced=forced)

    def timer_pause(self, *event):
        """Pause timer."""
        self.master.iconbitmap('icon_off.ico')
        self.time_to_end = self.end_time - datetime.now()
        self.abort = True
        self.btn_start.tkraise()
        self.btn_abort.config(style='buttons_timer.TButton')

    def timer_start(self, *event, ):
        """Start time.r"""
        if self.isactive:
            return None

        if not self.time_to_end:
            self.time_to_end = isotimestring2timedelta(self.time_setters.get_time_string(h_max=99, m_max=59, s_max=59))
            if not self.time_to_end:
                return None
            self.start_time = datetime.now()
        else:
            self.start_time = datetime.now()

        self.end_time = self.start_time + self.time_to_end

        self.master.iconbitmap('icon_on.ico')
        self.btn_pause.tkraise()
        self.btn_abort.config(style='red.buttons_timer.TButton')
        self.isactive = True

        def timer(compensation_delay=0):
            """Start timer watcher-daemon."""
            compensation_delay = self.master.COMPENSATION_DELAY
            while not self.abort:
                if self.time_to_end.total_seconds() <= compensation_delay:
                    self.master.who_is_ringing = '.!timerframe'
                    self.master.ring(compensation_delay=self.master.COMPENSATION_DELAY)
                    self.timer_abort(forced=True)
                else:
                    self.time_to_end = self.end_time - datetime.now()
                    if self.master.options.get('tab_id') == '.!timerframe':
                        self.master.set_countdown(self.time_to_end,
                                                  verbose=self.TIME_VERBOSE,
                                                  rounding=self.TIME_ROUNDING)
                sleep(self.UPDATE_INTERVAL)
            self.abort = False
            self.isactive = False
            if not self.master.is_any_active():
                self.master.iconbitmap('icon_off.ico')
        th = Thread(target=timer)
        th.daemon = True
        th.start()

        self.master.options.update({'last_used_timer': self.time_setters.get_time_string(h_max=99, m_max=59, s_max=59)})

    def update_timers(self):
        """Refresh user timers list."""
        self.timers = sorted(list(set(self.timers)), key=lambda x: isotimestring2timedelta(x))
        self.combo.config(values=self.timers)
        with open('timers.json', 'w') as file:
            json.dump(self.timers, file)
